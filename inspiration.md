## Inspiration
DeFi ecosystem, user experience and the simplicity are the focus of our projects.
Every day we listen about exchanges's scams, hight fees, hight interests, hidden and complex **intermediary systems** that works only to improve their conditions without thinking to users.
We love **programming**, we love **blockchain**, we love the **DeFi ideology** but we also love **make our future better than now** using technologies and our skills.
Many challanges are very difficult to face up, however we do our best to overcome hardles and a plus is the continuous improving of our **skills** and teamworks.
People who use their skills (strong or not) to build a **better future for our society** are our inspiration. 
 
## What it does
As you can see from image gallery, our idea concerns on receive a token (it’s name is _verified wallet_) that will be required to access to specific services like decentralised exchanges. The token will be send from a **whitelister authority**, such as regulated entities, through **Sigillum** website, only to the accounts that have been successfully completed **Know Your Customer** procedure on the platform.

**Algorand** offers the infrastructure on which token deliveries are recorded. **ASAs** offer an excellent tool for representing a token that will be used to identify users belonging to the **whitelist**. In particular, if a user has a token it means he will be whitelisted and if a user is whitelisted then he will be offered numerous opportunities and benefits from DeFi projects such as: **decentralised exchanges**, **decentralised estate plataforms**, **decentralised social media**, etc etc . . . .
Therefore, our token allow to understand if an account is _verified_ or not.  Every account must receive at most one token each, after **opt-in** for it. The **opt-in** procedure required to compile the **Know Your Customer** form.

We think that, especially when it comes to managing the "_public thing_", transparency is fundamental. Through the [**Algo Explorer**](https://algoexplorer.io) platform it is possible to view all the transactions that take place on the blockchain, verifying who owns a specific token or not.

Sigillum's platform provide three main sections:
1. **User section**;
2. **Admin panel**;
3. Decentralised **demo marketplace**.

**User section** allows users to compile **Know Your Customer** form and **opt-in** for our token.
**Admin panel** allows an **regulated entity** to whitelist a specific user sending him verified token, using the wallet address of the user. In addition, admin can see in a table the following information:
1. Who send all information or not and data that are loaded successfully or not;
2. Who has (or not) received the token, yet;

**Demo marketplace** is an exaple  of decentralised platform that required our tokens to login. In this way, only users that have been completed successfully KYC and opt-in procedure could login to DeFi platforms (will be explain better in the following sections). 


## How we built it
According to our inspirations and user experience, we build our project following the [**10 Usability Heuristics for User Interface Design** by Nielsen and Molich](https://www.nngroup.com/articles/ten-usability-heuristics/).
We made our platform as simple as possible in such a way **regulated entities** and users could trust us.

Using **Django framework** we have developed **back-end** and a **database** to store information about the users who made KYC procedure and Algorand’s wallets addresses.
Data are **docupled** using **hash functions**, so it won’t be possible to retrieve fiscal code knowing the wallet address or vice versa.  Nobody can know what is the relation between fiscal code and wallet addresses, ensuring the protection of sensitive data (according to GDPR).

Using **Python**, we have developed the logic of back-end who will comunicate with Algorand's blockchain because of user-admin actions. 
We designed a **connect Algorand's wallet** feature, in this way user could use our services using, for example, **Algorand wallet mobile app** or **myAlgo browser plugin**. Login and use of our logic will be fast and secure.

## Challenges we ran into
We ran into many difficult challenges such as:
1. **Algorand Wallet** connecting features. Because of it using **React framwork** (we didn't know it) it was difficult to integrate this function. We tried to workaround using **Pipeline UI** or **AlgoPay** but there was little time to study their documentation and, at the same time, design our project, developing front-end and back-end logic. Anyways, we do of our best.
2. **Time management**. Because of our University's engagements we have to organize our days to maximise our objective function: continue studying by following our university's lectures and make the best project that we could have done for this hackathon. 
3. **Programming languages**. We didn't realize a lot of projects in our career and our knowledge is limited. We use this opportunity to improve our skills.

Finally, we come across many difficult challenges such as design choices and bugs, but we tried to realize something we belived in, useful and simple. 

## Accomplishments that we're proud of
We are proud that we have realised something that works and could be use from users and regulated entities beacause of its simplicity.
We are proud of solve a problem with an innovative, secure, trasparent solution that is really operable in everyday life.
Finally, we are proud of done everithing and every key concept/idea that we have designed in the initial part of the project without omitting user experience and feasibility.

## What we learned
This Hackathon was a good opportunity to learn **team work** and **programming skills**.
To coordinate our work, we use **Trello**, and we have conciliate ideas of team's members.
This type of challenges allow us to improve our **friendship** because we have to work together to the solution to specific problem. We are really happy of that.
We improve our skills and knowledge of Python, Django, PostgreSQL, HTML, CSS, Javascript, Bootstrap, time management and, obviously, we learn more about Algorand and its potential.

## What's next for SIGILLUM
Improve the Algorand wallect feature.
Improvements of **Know Your Customer** procedure according to national legislation implementing the European directive such as the verification of:
1.   identification data;
2.  email addres;
3.  phone number;;
4.  geolocation;
5.  card identity;
6.  photo in real time;