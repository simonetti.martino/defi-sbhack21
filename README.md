# DeFi-SBHACK21
Our idea concerns on receive a token (its technical name is _verified wallet_) that will be required to access to specific services like decentralised exchanges. The token will be send from a **whitelister authority**, e.g. regulated entities, through **Sigillum** website, only to the accounts that have been successfully completed **Know Your Customer** procedure on the platform.

**Algorand** offers the infrastructure on which token deliveries are recorded. **ASAs** offer an excellent tool for representing a token that will be used to identify users belonging to the **whitelist**. In particular, if a user has a token it means he will be whitelisted and if a user is whitelisted then he will be offered numerous opportunities and benefits from DeFi projects such as: **decentralised exchanges**, **decentralised estate plataforms**, **decentralised social media**, etc etc . . . .
Therefore, our token allow to understand if an account is _verified_ or not.  Every account must receive at least one token each, after having **opt-in** for it. The **opt-in** procedure required to fill out the **Know Your Customer** form.

We strongly believe that, especially when it comes to managing the "_public thing_", transparency is fundamental. Through the [**Algo Explorer**](https://algoexplorer.io) platform it is possible to view all the transactions that take place on the blockchain, verifying who owns a specific token or not.

Sigillum's platform provides three main sections:
1. **User section**;
2. **Admin panel**;
3. Decentralised **demo marketplace**.

## User manual

1. Clone the repo: 
    `git clone  https://gitlab.com/simonetti.martino/defi-sbhack21.git `
2. In the directory /defi-sbhack21/DeFi/ create a virtual environment and start it (you need virtualenv):

   `virtualenv sig_env`
   
   `source sig_env/bin/activate`

2. Install requirements.txt file:

   `pip install -r requirements.txt`

3. Run (PostgreSQL instance must running): 

    `python manage.py makemigrations`
    
    `python manage.py migrate`

    `python manage.py runserver`

## Setting the database

Create a new server in PostgreSQL and then a database with name: contest

Open /defi-sbhack21/DeFi/DeFi/settings.py file and read the parameters of database. The name, host, port and user and password must match to your instance of your database on postgresql.
