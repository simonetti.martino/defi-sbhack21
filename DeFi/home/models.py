from django.db import models
from django.conf import settings

# Create your models here.


class UtenteRegister(models.Model):
    fiscal_code = models.CharField(max_length=250, blank=True, null=False)
    wallet_address = models.CharField(max_length=250, blank=True, null=False, unique=True)
    def __str__(self):
        return 'fiscal_code:{}, wallet_address:{}'.format(self.fiscal_code,
                                                          self.wallet_address)


class KnowYourCostumerData(models.Model):
    fiscal_code = models.CharField(max_length=300, blank=True, null=False,unique=True)
    name = models.CharField(max_length=250, blank=True, null=False)
    surname = models.CharField(max_length=250, blank=True, null=False)
    age = models.IntegerField()
    token_rcvd = models.BooleanField(default=False)
    def __str__(self):
        return 'fiscal_code:{}, name:{}, surname:{}, age:{}, token received:{}'.format(self.fiscal_code,
                                                                                        self.name,
                                                                                        self.surname,
                                                                                        self.age,
                                                                                        self.token_rcvd)
