from django.forms import ModelForm
from .models import UtenteRegister, KnowYourCostumerData
from django import forms
from django.forms import ModelForm, TextInput, NumberInput


class UtenteRegisterForm(ModelForm):
	class Meta:
		model = UtenteRegister
		fields = '__all__'

class KnowYourCostumerDataForm(ModelForm):
	class Meta:
		model = KnowYourCostumerData
		fields = '__all__'
	fiscal_code = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Fiscal Code...', 'style': 'width: 500px;', 'class': 'form-control'}))	    
	name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Name...', 'style': 'width: 500px;', 'class': 'form-control'}))
	surname = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Surname...', 'style': 'width: 500px;', 'class': 'form-control'}))
	age = forms.CharField(widget=forms.NumberInput(attrs={'placeholder': 'Age...', 'style': 'width: 500px;', 'class': 'form-control'}))
	passphase = forms.CharField(widget=forms.Textarea(attrs={'rows':5,'cols':20,'placeholder': 'Passphase...', 'style': 'width: 500px;', 'class': 'form-control'}))


class PassphaseForm(forms.Form):
	passhphase_login = forms.CharField(widget=forms.Textarea(attrs={'rows':5,'cols':20,'placeholder': 'Insert your passphase to login...', 'style': 'width: 500px;', 'class': 'form-control'}))
