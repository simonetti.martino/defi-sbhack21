from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    #path('index/', views.index, name='index'),
    path('logout/', views.logout, name='logout'),
    path('defi/', views.defi, name='defi'),
    path('kyc/', views.kyc, name='kyc'),
    path('has_token/', views.has_token, name='has_token'),
    path('send_token/',views.send_token, name='send_token'),
    path('administrator/', views.administrator, name='administrator'),
    path('marketplace/', views.marketplace, name='marketplace')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)