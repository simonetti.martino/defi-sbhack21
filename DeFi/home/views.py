from django.shortcuts import render, redirect
from . import views
from django.contrib import messages
import requests
import json
from django.db import connection, IntegrityError
from django.core.serializers import serialize
from home.serializers import UtenteRegister_serializer, KnowYourCostumerData_serializer
from django.http import HttpResponseRedirect
from home.models import UtenteRegister, KnowYourCostumerData
from .forms import UtenteRegisterForm,KnowYourCostumerDataForm, PassphaseForm
from algosdk import account, mnemonic, error, future
from algosdk.v2client import algod
import hashlib


#login to marketplace, you need the token
def login(request):
    form = PassphaseForm()
    if request.method == 'POST':
        form = PassphaseForm(request.POST)
        if form.is_valid():
            passphase = request.POST.get('passhphase_login','')
            public_key = mnemonic.to_public_key(passphase)
            if(check_token_in_wallet(public_key)):
                request.session['Accedi'] = True
                context = {'form':form, 'accedi': request.session['Accedi']}
                print("you are entering")
                return render(request, 'marketplace.html',context)
            else:
                request.session['Accedi'] = False
                context = {'form':form, 'accedi': request.session['Accedi']}
                print("you cannot enter")  
                return render(request, 'login.html',context)

    context = {'form':form, 'accedi': request.session['Accedi']}
    return render(request, 'login.html',context)


def index(request):
    return render(request, 'index.html')

def defi(request):
    return render(request, 'defi.html')


def check_token_in_wallet(pub_key): #check if a wallet has the token 39156404
    response = requests.get("https://testnet.algoexplorerapi.io/v2/accounts/" + pub_key)
    fileJson = json.loads(response.text)
    amaut = 0
    for e in fileJson['assets']:
        if(e['asset-id'] == 39156404):
            amaut = e['amount']
    if(amaut >= 1):
        return True
    else :
        return False

def administrator(request):
    dati = 'no data available'
    kyc_data = KnowYourCostumerData.objects.all()
    serializerKYC = KnowYourCostumerData_serializer(kyc_data, many=True)
    count = 0
    while count < len(serializerKYC.data):
        #recover the fiscal code
        cf = serializerKYC.data[count]['fiscal_code']
        #in the DB the fiscal code is hashed, so for compare them, we have to hash it 
        cf_encoded = cf.encode()
        cf_hash = hashlib.sha256(cf_encoded)
        cf_hex = cf_hash.hexdigest()
        #compare the two fiscal code
        wallet_addr_query = UtenteRegister.objects.filter(fiscal_code=cf_hex)
        wallet_serializer = UtenteRegister_serializer(wallet_addr_query, many=True)
        #get the wallet associated to fiscal code
        wallet_address = wallet_serializer.data[0]['wallet_address']
        #CONTROLLO SE L'UTENTE POSSIEDE QUEL TOKEN
        if (check_token_in_wallet(wallet_address)):
            print('you have the token')
            serializerKYC.data[count]['token_rcvd'] = "true"
        else:
            print('you do not have the token')
            serializerKYC.data[count]['token_rcvd'] = "false"
        count = count + 1

    json_data_kyc = json.dumps(serializerKYC.data, default=lambda o: o.__dict__, indent=4)

    context = {'dati': json_data_kyc}
    return render(request, 'administrator.html',context)

def marketplace(request):
    if(request.session['Accedi']):
        return render(request, 'marketplace.html')
    else:
        return HttpResponseRedirect('/login/')

def has_token(request):
    request.session['Accedi'] = False
    try:
        passhase = request.POST.get('public_key','')
        public_key = mnemonic.to_public_key(passhase)
        print(public_key)
        response = requests.get("https://testnet.algoexplorerapi.io/v2/accounts/" + public_key)
        fileJson = json.loads(response.text)
        amaut = 0
        for e in fileJson['assets']:
            # if(e['asset-id'] == 39156404):   # ... asset per l'HACKATHON
            if(e['asset-id'] == 39156404):
                amaut = e['amount']
                print(amaut)
        
        if(amaut == 1):
            request.session['Accedi'] = True
            ret = redirect('marketplace')
        else :
            messages.add_message(request, messages.ERROR, 'You have not the pass')
            ret = redirect('defi')
    except error.WrongMnemonicLengthError:
        messages.add_message(request, messages.ERROR, 'The Algorand passphrase is not valid!')
        ret = redirect('defi')
    return ret



def kyc(request):
    try:
        # if this is a POST request we need to process the form data
        form = KnowYourCostumerDataForm()
        if request.method == 'POST':
            # create a form instance and populate it with data from the request:
            form = KnowYourCostumerDataForm(request.POST)
            # check whether it's valid:
            if form.is_valid():
                # ----------------- START OPT-IN -----------------
                fiscal_c = request.POST.get('fiscal_code','')
                # get the master derivation key from the mnemonic
                #backup = "such chapter crane ugly uncover fun kitten duty culture giant skirt reunion pizza pill web monster upon dolphin aunt close marble dune kangaroo ability merit"
                passhase = request.POST.get('passphase','')
                
                public_key = mnemonic.to_public_key(passhase)
                encoded = fiscal_c.encode()
                fiscal_code_hash = hashlib.sha256(encoded)
                utente_register = UtenteRegister(fiscal_code=fiscal_code_hash.hexdigest(), wallet_address=public_key)
                
                private_key = mnemonic.to_private_key(passhase)

                algod_address = "https://testnet-algorand.api.purestake.io/ps2/"
                algod_token = ""

                headers = {
                "x-api-key": "8xIHOoT49CyCFX6PLJQF2Ey1nJJczJW2j3A0DrVj"
                }
                
                algod_client = algod.AlgodClient(algod_token, algod_address, headers);
                
                asset_id = 39156404
                
                params = algod_client.suggested_params() 
                # comment these two lines if you want to use suggested params
                params.fee = 1000
                params.flat_fee = True
                
                account_info = algod_client.account_info(public_key)
                holding = None
                idx = 0
                for my_account_info in account_info['assets']:
                    scrutinized_asset = account_info['assets'][idx]
                    idx = idx + 1    
                    if (scrutinized_asset['asset-id'] == asset_id):
                        holding = True
                        break

                if not holding:
                    # Use the AssetTransferTxn class to transfer assets and opt-in
                    txn = future.transaction.AssetTransferTxn(
                        sender=public_key,
                        sp=params,
                        receiver=public_key,
                        amt=0,
                        index=asset_id)
                    
                    stxn = txn.sign(private_key)
                    print("stxn ",stxn)
                    txid = algod_client.send_transaction(stxn)
                    print("================ ",txid)
                
                #check if the public key already exisist in the table
                query_check_pubkey = UtenteRegister.objects.filter(wallet_address=public_key)
                if query_check_pubkey: #if the return is not empty, i can't insert the KYC
                    messages.add_message(request, messages.ERROR, 'Your wallet address already exist')
                    ret = redirect('kyc')
                else:
                    form.save() #va salvato solo se il form è valido
                    utente_register.save()
            else:
                messages.add_message(request, messages.ERROR, 'Your fiscal code already exist')
                ret = redirect('kyc')
            #---------------------- END OPT-IN ----------------
            
            #messages.add_message(request, messages.SUCCESS, 'Your data have been submitted')
            #clean the form
            form = KnowYourCostumerDataForm()
            # redirect to a new URL:
            #return HttpResponseRedirect('/thanks/')
        
        context = {'form':form}
        #if request.session['Accedi']:
        ret = render(request, 'kyc.html', context)
        #else:
            #ret = render(request, 'login.html', context)
    except error.WrongMnemonicLengthError:
        messages.add_message(request, messages.ERROR, 'The Algorand passphrase is not valid!')
        ret = redirect('kyc')
    except error.AlgodHTTPError:
        messages.add_message(request, messages.ERROR, 'Probably your Algorand wallet has not enaugh algos.')
        ret = redirect('kyc')
    except IntegrityError:
        messages.add_message(request, messages.ERROR, 'Your wallet address already exist')
        ret = redirect('kyc')

    return ret




def send_token(request):
    try:
        # get the master derivation key from the mnemonic
        #backup = "such chapter crane ugly uncover fun kitten duty culture giant skirt reunion pizza pill web monster upon dolphin aunt close marble dune kangaroo ability merit"
        passhase = request.POST.get('passphase','')
        fiscal_code_user = request.POST.get('accout','')
        #hash of fiscal code
        cf_encoded = fiscal_code_user.encode()
        cf_hash = hashlib.sha256(cf_encoded)
        cf_hex = cf_hash.hexdigest()

        query_cf_hex = UtenteRegister.objects.filter(fiscal_code=cf_hex)
        #serialization of query results
        wallet_serializer = UtenteRegister_serializer(query_cf_hex, many=True)
        #get the wallet associated to fiscal code
        account_ricevente = wallet_serializer.data[0]['wallet_address']


        account_creatore = {}
        account_creatore['chiave_pubblica'] = mnemonic.to_public_key(passhase)
        account_creatore['chiave_privata'] = mnemonic.to_private_key(passhase)
        if (account_creatore['chiave_pubblica'] == "Y6UVWTD4XV6X5H3M44ELCB5QSFOOMW2YCOS2GFCHML5WSHJK4P74JY7L2Q"):

            algod_address = "https://testnet-algorand.api.purestake.io/ps2/"
            algod_token = ""

            headers = {
            "x-api-key": "8xIHOoT49CyCFX6PLJQF2Ey1nJJczJW2j3A0DrVj"
            }
            
            algod_client = algod.AlgodClient(algod_token, algod_address, headers);
            
            asset_id = 39156404
            
            params = algod_client.suggested_params() 
            # comment these two lines if you want to use suggested params
            params.fee = 1000
            params.flat_fee = True
            

            # DEFREEZ
            txn_1 = future.transaction.AssetFreezeTxn(
                sender=account_creatore['chiave_pubblica'],
                sp=params,
                index=asset_id,
                target=account_ricevente,
                new_freeze_state=False   
                )

            # TRANSFER ASSET
            # transfer asset of 10 from account 1 to account 3
            params = algod_client.suggested_params()
            # comment these two lines if you want to use suggested params
            params.fee = 1000
            params.flat_fee = True
            txn_2 = future.transaction.AssetTransferTxn(
                sender=account_creatore['chiave_pubblica'],
                sp=params,
                receiver=account_ricevente,
                amt=1,
                index=asset_id)

            # txid = algod_client.send_transaction(stxn)
            #print(txid)
            # Wait for the transaction to be confirmed
            #wait_for_confirmation(algod_client, txid)
            # The balance should now be 10.
            #print_asset_holding(algod_client, account_ricevente['chiave_pubblica'], asset_id)

            # DEFREEZ

            txn_3 = future.transaction.AssetFreezeTxn(
                sender=account_creatore['chiave_pubblica'],
                sp=params,
                index=asset_id,
                target=account_ricevente,
                new_freeze_state=True   
                )

            # get group id and assign it to transactions
            gid = future.transaction.calculate_group_id([txn_1, txn_2, txn_3])
            txn_1.group = gid
            txn_2.group = gid
            txn_3.group = gid

            stxn_1 = txn_1.sign(account_creatore['chiave_privata'])
            stxn_2 = txn_2.sign(account_creatore['chiave_privata'])
            stxn_3 = txn_3.sign(account_creatore['chiave_privata'])

            # assemble transaction group
            signed_group =  [stxn_1, stxn_2, stxn_3]

            algod_client.send_transactions(signed_group)      
            
            ret = redirect('administrator')
        else:
            messages.add_message(request, messages.ERROR, 'This is not the Administrator account!')
            ret = redirect('administrator')
        
    except error.WrongMnemonicLengthError:
        messages.add_message(request, messages.ERROR, 'The Algorand passphrase is not valid!')
        ret = redirect('administrator')
    except error.AlgodHTTPError:
        messages.add_message(request, messages.ERROR, 'Probably your Algorand wallet has not enaugh algos.')
        ret = redirect('administrator')
    
        
    
    
    return ret



def logout(request):
    request.session['Accedi'] = False
    ACCEDI = request.session['Accedi']
    context = {'accedi': ACCEDI}
    return render(request, 'logout.html', context)

