from rest_framework import serializers
from home.models import UtenteRegister, KnowYourCostumerData

class UtenteRegister_serializer(serializers.ModelSerializer):
    class Meta:
        model = UtenteRegister
        fields = ['fiscal_code','wallet_address']

class KnowYourCostumerData_serializer(serializers.ModelSerializer):
    class Meta:
        model = KnowYourCostumerData
        fields = '__all__'